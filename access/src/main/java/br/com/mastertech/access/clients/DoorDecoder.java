package br.com.mastertech.access.clients;

import br.com.mastertech.access.exceptions.CustomerNotFoundException;
import br.com.mastertech.access.exceptions.DoorNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class DoorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404)
            return new DoorNotFoundException();
        return errorDecoder.decode(s, response);
    }
}
