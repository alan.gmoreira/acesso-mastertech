package br.com.mastertech.access.repositories;

import br.com.mastertech.access.models.Access;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AccessRepository extends CrudRepository<Access, Integer> {

    Optional<Access> findByCustomerIdAndDoorId(int customerId, int doorId);
}
