package br.com.mastertech.access.controllers;

import br.com.mastertech.access.models.Access;
import br.com.mastertech.access.models.dto.AccessMapper;
import br.com.mastertech.access.models.dto.CreateAccessRequestResponse;
import br.com.mastertech.access.services.AccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AccessController {

    @Autowired
    private AccessService accessService;

    @Autowired
    private AccessMapper accessMapper;

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
    public CreateAccessRequestResponse createAccess(@RequestBody CreateAccessRequestResponse createAccessRequestResponse)
    {
        Access access = accessService.createAccess(accessMapper.toAccess(createAccessRequestResponse));

        return accessMapper.toCreateAccessRequestResponse(access);
    }

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping("/{cliente_id}/{porta_id}")
    public void deleteAccess(@PathVariable int cliente_id, @PathVariable int porta_id)
    {
        accessService.deleteAccess(cliente_id, porta_id);
    }

    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @GetMapping("/{cliente_id}/{porta_id}")
    public CreateAccessRequestResponse getAccess(@PathVariable int cliente_id, @PathVariable int porta_id)
    {
        return accessMapper.toCreateAccessRequestResponse(accessService.getAccess(cliente_id, porta_id));
    }
}
