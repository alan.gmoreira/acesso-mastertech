package br.com.mastertech.access.clients;

import br.com.mastertech.access.exceptions.DoorNotAvailableException;

public class DoorClientFallBack implements DoorClient{
    @Override
    public Door getDoor(int id) {
        throw new DoorNotAvailableException();
    }
}
