package br.com.mastertech.access.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateAccessRequestResponse {

    @JsonProperty("porta_id")
    private int doorId;

    @JsonProperty("cliente_id")
    private int customerId;

    public CreateAccessRequestResponse() {
    }

    public int getDoorId() {
        return doorId;
    }

    public void setDoorId(int doorId) {
        this.doorId = doorId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
