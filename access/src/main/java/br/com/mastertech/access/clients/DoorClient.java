package br.com.mastertech.access.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/porta")
@FeignClient(name = "DOOR", configuration = DoorConfiguration.class)
public interface DoorClient {

    @GetMapping("/{id}")
    Door getDoor(@PathVariable int id);

}
