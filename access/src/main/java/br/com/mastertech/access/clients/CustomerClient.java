package br.com.mastertech.access.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CUSTOMER", configuration = CustomerConfiguration.class)
public interface CustomerClient {

    @GetMapping("/cliente/{clientId}")
    Customer getById(@PathVariable int clientId);

}
