package br.com.mastertech.access.models.dto;

import br.com.mastertech.access.models.Access;
import org.springframework.stereotype.Component;

@Component
public class AccessMapper {

    public CreateAccessRequestResponse toCreateAccessRequestResponse(Access access)
    {
        CreateAccessRequestResponse createAccessRequestResponse = new CreateAccessRequestResponse();

        createAccessRequestResponse.setCustomerId(access.getCustomerId());
        createAccessRequestResponse.setDoorId(access.getDoorId());

        return createAccessRequestResponse;
    }

    public Access toAccess(CreateAccessRequestResponse createAccessRequestResponse)
    {
        Access access = new Access();

        access.setCustomerId(createAccessRequestResponse.getCustomerId());
        access.setDoorId(createAccessRequestResponse.getDoorId());

        return access;
    }
}
