package br.com.mastertech.access.clients;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class DoorConfiguration {

    @Bean
    public ErrorDecoder getDoorDecoder() {
        return new DoorDecoder();
    }

    @Bean
    public Feign.Builder builder()
    {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new DoorClientFallBack(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
