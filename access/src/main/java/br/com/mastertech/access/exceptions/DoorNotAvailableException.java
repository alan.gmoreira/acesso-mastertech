package br.com.mastertech.access.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "Door API unavailable!")
public class DoorNotAvailableException extends RuntimeException {
}
