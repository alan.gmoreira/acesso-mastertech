package br.com.mastertech.access.services;

import br.com.mastertech.access.clients.Customer;
import br.com.mastertech.access.clients.CustomerClient;
import br.com.mastertech.access.clients.Door;
import br.com.mastertech.access.clients.DoorClient;
import br.com.mastertech.access.exceptions.AccessNotFoundException;
import br.com.mastertech.access.models.Access;
import br.com.mastertech.access.repositories.AccessRepository;
import org.omg.CORBA.CustomMarshal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccessService {

    @Autowired
    private AccessRepository accessRepository;

    @Autowired
    private CustomerClient customerClient;

    @Autowired
    private DoorClient doorClient;

    public Access createAccess(Access access){

        Customer customer = customerClient.getById(access.getCustomerId());

        Door door = doorClient.getDoor(access.getDoorId());

        return accessRepository.save(access);
    }

    public Access getAccess(int customerId, int doorId){
        Optional<Access> optionalAccess = accessRepository.findByCustomerIdAndDoorId(customerId, doorId);

        if (optionalAccess.isPresent())
            return  optionalAccess.get();

        throw new AccessNotFoundException();
    }

    public void deleteAccess(int customerId, int doorId)
    {
        Access access = getAccess(customerId, doorId);

        accessRepository.delete(access);
    }

}
