package br.com.mastertech.access.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Access {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int accessId;

    private int doorId;

    private int customerId;

    public Access() {
    }

    public int getAccessId() {
        return accessId;
    }

    public void setAccessId(int accessId) {
        this.accessId = accessId;
    }

    public int getDoorId() {
        return doorId;
    }

    public void setDoorId(int portaId) {
        this.doorId = portaId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int clienteId) {
        this.customerId = clienteId;
    }
}
