package br.com.mastertech.access.clients;

import br.com.mastertech.access.exceptions.CustomerNotAvailableException;

public class CustomerClientFallBack implements CustomerClient{

    @Override
    public Customer getById(int clientId) {
        throw new CustomerNotAvailableException();
    }
}
