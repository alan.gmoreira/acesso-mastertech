package br.com.mastertech.access.clients;

public class Door {

    private int id;

    private int floor;

    private String roon;

    public Door() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getRoon() {
        return roon;
    }

    public void setRoon(String roon) {
        this.roon = roon;
    }
}
