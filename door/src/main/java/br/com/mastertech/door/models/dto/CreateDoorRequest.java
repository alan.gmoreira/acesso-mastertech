package br.com.mastertech.door.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateDoorRequest {

    @JsonProperty("andar")
    private int floor;

    @JsonProperty("sala")
    private String roon;

    public CreateDoorRequest() {
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getRoon() {
        return roon;
    }

    public void setRoon(String roon) {
        this.roon = roon;
    }
}
