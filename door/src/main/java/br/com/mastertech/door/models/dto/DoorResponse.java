package br.com.mastertech.door.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DoorResponse {

    private int id;
    @JsonProperty("andar")
    private int floor;
    @JsonProperty("sala")
    private String roon;

    public DoorResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getRoon() {
        return roon;
    }

    public void setRoon(String roon) {
        this.roon = roon;
    }
}
