package br.com.mastertech.door.models.dto;

import br.com.mastertech.door.models.Door;
import org.springframework.stereotype.Component;

@Component
public class DoorMapper {

    public Door toDoor(CreateDoorRequest createDoorRequest)
    {
        Door door = new Door();
        door.setFloor(createDoorRequest.getFloor());
        door.setRoon(createDoorRequest.getRoon());

        return door;
    }

    public DoorResponse toDoorResponse(Door door)
    {
        DoorResponse doorResponse = new DoorResponse();

        doorResponse.setFloor(door.getFloor());
        doorResponse.setId(door.getId());
        doorResponse.setRoon(door.getRoon());

        return doorResponse;
    }
}
