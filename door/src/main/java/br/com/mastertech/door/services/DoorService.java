package br.com.mastertech.door.services;

import br.com.mastertech.door.Exceptions.DoorNotFoundException;
import br.com.mastertech.door.models.Door;
import br.com.mastertech.door.repositories.DoorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Optional;

@Service
public class DoorService {

    @Autowired
    private DoorRepository doorRepository;

    public Door createDoor(Door door)
    {
        door.setId(0);
        return doorRepository.save(door);
    }

    public Door getDoorById(int id)
    {
        Optional<Door> optionalDoor =  doorRepository.findById(id);
        if(optionalDoor.isPresent())
            return optionalDoor.get();

        throw new DoorNotFoundException();
    }

}
