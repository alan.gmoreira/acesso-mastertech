package br.com.mastertech.door.repositories;

import br.com.mastertech.door.models.Door;
import org.springframework.data.repository.CrudRepository;

public interface DoorRepository extends CrudRepository<Door, Integer> {

}
