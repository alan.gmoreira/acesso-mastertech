package br.com.mastertech.door.controller;

import br.com.mastertech.door.models.Door;
import br.com.mastertech.door.models.dto.CreateDoorRequest;
import br.com.mastertech.door.models.dto.DoorMapper;
import br.com.mastertech.door.models.dto.DoorResponse;
import br.com.mastertech.door.services.DoorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class DoorController {

    @Autowired
    private DoorService doorService;

    @Autowired
    private DoorMapper doorMapper;

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
    public DoorResponse createDoor(@RequestBody @Valid CreateDoorRequest createDoorRequest) {
         Door door = doorService.createDoor(doorMapper.toDoor(createDoorRequest));

         return doorMapper.toDoorResponse(door);
    }

    @GetMapping("/{id}")
    public DoorResponse getDoor(@PathVariable int id) {
        Door door = doorService.getDoorById(id);

        return doorMapper.toDoorResponse(door);
    }
}
