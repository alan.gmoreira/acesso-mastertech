package br.com.mastertech.customer.controllers;

import br.com.mastertech.customer.models.Customer;
import br.com.mastertech.customer.models.dto.CreateCustomerRequest;
import br.com.mastertech.customer.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Customer createCustomer(@RequestBody @Valid CreateCustomerRequest createCustomerRequest) {
        return customerService.cadastrarCliente(createCustomerRequest.getName());
    }

    @GetMapping("/{id}")
    public Customer getbyId(@PathVariable int id) {
        return customerService.buscaPorId(id);
    }
}
